import { dataGlasses } from "./data.js";

const showGlasses = (dataGlasses) => {
  let glassesDisplay = "";
  dataGlasses.forEach((glasses) => {
    glassesDisplay += `
      <div class="col-4">
        <img id="${glasses.id}" class="img-fluid" src="${glasses.src}"/>
      </div>
    `;
  });
  document.querySelector("#vglassesList").innerHTML = glassesDisplay;
};

const addGlassesToModel = (glasses) => {
  document.querySelector(
    "#avatar"
  ).innerHTML = `<img src="${glasses.virtualImg}" alt="" />`;
};

const showGlassDetail = (glasses) => {
  let detailMarkup = `
    <h2 class="mb-3">${glasses.name.toUpperCase()} - ${glasses.brand} (${
    glasses.color
  })</h2>
    <div class="mb-4">
      <span class="mr-3">$${glasses.price}</span>
      <span>Stocking</span>
    </div>
    <p>
      ${glasses.description}
    </p>
  `;

  document.querySelector(".vglasses__info").style.display = "block";
  document.querySelector("#glassesInfo").innerHTML = detailMarkup;
};

const removeModelCurrentGlass = () => {
  if (document.querySelector("#avatar > img")) {
    document.querySelector("#avatar > img").style.display = "none";
  }
};

const showModelCurrentGlass = () => {
  if (document.querySelector("#avatar > img")) {
    document.querySelector("#avatar > img").style.display = "inline";
  }
};

document.querySelector("#vglassesList").addEventListener("click", (event) => {
  if (event.target.tagName !== "IMG") {
    return;
  }

  let targetGlass = dataGlasses.find(
    (glasses) => glasses.id === event.target.id
  );
  addGlassesToModel(targetGlass);
  showGlassDetail(targetGlass);
});

document.querySelector("#btnBefore").addEventListener("click", () => {
  removeModelCurrentGlass();
});

document.querySelector("#btnAfter").addEventListener("click", () => {
  showModelCurrentGlass();
});

showGlasses(dataGlasses);
